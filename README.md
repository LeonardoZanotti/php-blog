# Blog em PHP
Este projeto consiste em um blog simples, feito com PHP, HTML, CSS e JavaScript, além de possuir integração com banco de dados MySQL e uso das CDNs do Bootstrap e JQuery.

É possível visualizar uma rápida explicação do projeto clicando [aqui](https://youtu.be/_eg-ddI6gf4).

![Admin user](./img/README/blog-admin.png "Admin user")


## Getting started
Inicialmente, é preciso clonar o repositório para sua máquina local.

Após isso, entre na pasta do projeto, então, entre na pasta `database`. Nesta pasta, há um arquivo chamada `credentials_example.php`. Crie uma cópia deste arquivo e renomeie a cópia para `credentials.php`. Após feito isso, preencha as variáveis `$servername`, `$db_username` e `$db_password` com os dados da sua conexão MySQL.

![Credenciais](img/README/credentials.png "Credenciais")

Para rodar a aplicação localmente, utilize um servidor como [Nginx](https://www.nginx.com/) ou [Apache](https://www.apache.org/), ou ainda, usando o seguinte comando dentro da pasta do projeto:
```
$ php -S localhost:8000     # o projeto estará rodando em localhost:8080
```

## Estrutura do banco de dados
![Estrutura do banco de dados](./img/README/database.png "Estrutura do banco de dados")

O banco de dados é nomeado "php_blog" e possui apenas 3 tabelas: users, categories e posts. A tabela users é responsável por guardar os dados dos usuários do sistema, que incluem email, nome, senha e se é administrador. A tabela categories armazena dados das categorias (nome). Por fim, posts é a tabela das postagens do blog, tendo informações de nome, descrição, imagem e id da categoria do post. Desse modo, a tabela de categorias possui relacionamento 1:N com a tabela de posts. Além disso, todas as tabelas possuem, é claro, id e timestamps.


## Estrutura de pastas
![Estrutura das pastas](./img/README/folders.png "Estrutura das pastas")

Dentro do repositório do projeto existem 4 pastas:
- `database`: Possui todos os scripts para criar o banco de dados e semeá-lo.
- `img`: Guarda todas as imagens dos posts criados, além das imagens do README.
- `pages`: Possui os arquivos das páginas do site, de modo que possui uma subpasta para cada página.
- `utils`: Arquivos de uso geral, como funções úteis a mais de um arquivo.


## Setando banco de dados
Após ter o projeto rodando, abra-o no navegador. Uma página como a seguinte será mostrada:

![Initial page](./img/README/initial-page.png "Página inicial")

Então, entre na pasta database e clique no arquivo chamado `migrate.php` (ou apenas adicione `/database/migrate.php` à url). Você deverá visualizar a seguinte página:

![migrate.php](./img/README/connect.png "migrate.php")

Isto significa que, respectivamente:
- Foi possível conectar ao MySQL.
- O banco de dados "php_blog" foi apagado, caso existisse.
- O banco de dados "php_blog" foi criado.
- Foi possível se conectar o banco de dados php_blog.
- Foi possível criar a tabela de categorias.
- Foi possível semear a tabela de categorias com as categorias: "Games", "Books", "Movies", "Cooking" e "Sports".
- Foi possível criar a tabela de posts.
- Foi possível semear a tabela de posts, relacionando-os com as categorias.
- Foi possível criar a tabela de usuários.
- Foi possível semear a tabela de usuários, criando um usuário admin e um usuário comum, ambos com senha padrão "123456".

## Workflow
O site desenvolvido possui 5 páginas apenas: Login, Registro, Blog, Criação de posts e Edição de posts.

A navegação entre elas se dá conforme a figura abaixo:

![Workflow](./img/README/navigation.png "Navegação")

### Login
Página inicial do site, onde é realizado login. Os scripts de criação do banco criam dois usuários padrão que podem ser usados para testar a aplicação, ambos possuem como senha "123456":
- admin@gmail.com
- user@gmail.com

![Login](./img/README/login.png "Login")

Nesta página foram feitas validações nos campos de modo a garantir que os mesmos sejam preenchidos, tais como: validação de email válido, verificação se o usuário realmente existe e caso exista, verificação se a senha está correta.

A página conta, ainda, com uma checkbox "Remember me", a qual possui a finalidade de manter o usuário logado por um período de tempo maior. Quando se faz login com ela desmarcada, é garantido um token via cookie que dura 1 hora. Quando este cookie expira, é necessário logar novamente. Por outro lado, caso seja feito login com o remember me marcado, esse cookie possui duração de 1 semana. Além disso, toda vez que o usuário entra na plataforma a expiração do cookie é prolongada por mais uma semana.

### Registro
![Registro](./img/README/register.png "Registro")

Para se registrar, deve-se preencher os campos: Nome de usuário, Email, Senha e Confirmar senha, de modo que o email seja válido e não utilizado por outro usuário, a senha e sua confirmação sejam iguais e demais campos estejam preenchidos. O botão de remember me funciona da mesma forma que na página de login.

### Blog
Nesta página temos duas possíveis visualizações:
- Usuário comum, o qual possui a visualização dos posts, categorias e opção de logout
![Blog comum](./img/README/blog-common.png "Blog comum")

- Usuário admin, o qual consegue acessar os mesmos items que o usuário comum, com a adição dos botões de criar, editar e apagar posts.
![Blog admin](./img/README/blog-admin.png "Blog admin")

Em geral, a página faz a listagens dos posts do banco de dados.

No canto superior esquerdo, no menu de categorias, é possível filtrar os posts por categoria.

![Filtrar por categoria](img/README/blog-category.png "Filtrar por categoria")

No canto superior direito, é possível fazer logout, destruindo a sessão e cookie.

Clicando no botão "Create post" o usuário é redirecionado para a página de criar posts.

Clicando no botão "Edit post" o usuário é redirecionado para a página de edição do respectivo post.

### Criar post (Admin)
![Criar post](img/README/create-post.png "Criar post")

Para criar um post, o usuário admin deve informar o título do post, bem como sua descrição e nome de uma categoria existente, além de selecionar uma imagem.

A imagem selecionada será salva dentro da pasta `imgs/posts` com um nome único, devendo ser do tipo jpg, jpeg ou png e não ultrapassando o limite de 1MB.

Ao criar o post, a data de criação e edição do post serão marcadas como a atual.

### Editar post (Admin)
![Edit post](img/README/edit-post.png "Editar post")

Para editar um post, o usuário admin pode alterar qualquer atributo do mesmo (título, descrição, categoria e imagem). Independentemente do que mudar, a data de edição do post será alterada para a atual. Caso seja selecionada outra imagem, a imagem antiga será apagada e a nova será salva em seu lugar, com outro nome único, devendo ser do tipo jpg, jpeg ou png e não ultrapassando o limite de 1MB.

### Deletar post (Admin)
![Delete post](img/README/delete-post.png "Deletar post")

Na página blog, ao clicar no botão vermelho com ícone de lixeira, aparecerá um modal para confirmar a deleção do post. Caso o usuário clique em "OK", o post será deletado, caso contrário, nada acontecerá.

Ao deletar o post, o mesmo será apagado para sempre (muito tempo) do banco de dados, bem como sua imagem será deletada da pasta em que estava.

## Roadmap
Apesar do projeto cumprir os requisitos exigidos, acredito que, principalmente devido à falta de tempo, não consegui implementar algumas features que tinha em mente, tais como: sistema de comentários para os usuários, CRUD de categorias, CRUD de About Me (painel direito da página blog), melhorias na segurança geral do site (pois há graves problemas de SQL Injection e XSS), melhorias de UI e UX, entre outros.

## Authors and acknowledgment
Este projeto foi feito integralmente pelo aluno Leonardo José Zanotti, matriculado sob o GRR20204442.