<?php
require_once('../../utils/show_errors.php');
require_once('../../utils/authenticated.php');
require_once('../../database/db_functions.php');

if (!isAuthenticated()) {
    session_unset();
    session_destroy();
    header("Location: " . "../login/login.php");
}

if (isset($_SESSION["remember"]) && $_SESSION["remember"]) {
    setcookie("remember", "true", time() + (3600 * 24 * 7), "/");
}

$conn = connect_database();
$categories_sql = "SELECT category_id, category_name FROM $table_categories;";

$category_query = isset($_GET["category"]) ? "WHERE category_id = " . mysqli_real_escape_string($conn, htmlspecialchars($_GET["category"])) : '';
$posts_sql = "SELECT * FROM $table_posts $category_query;";

$categories_result = mysqli_query($conn, $categories_sql);
$posts_result = mysqli_query($conn, $posts_sql);
disconnect_db($conn);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="./blog.css">
    <link rel="stylesheet"
        href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
    <link rel="stylesheet"
        href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p"
        crossorigin="anonymous" />
    <script
        src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js">
    </script>
    <script
        src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js">
    </script>

    <title>PHP Blog</title>
</head>

<body>
    <div class="header">
        <h2>PHP Blog</h2>
        <div class="logout">
            <a type="button" href="./actions/logout.php"
                class="btn btn-default">Logout
                (<?= $_SESSION["user_name"]; ?>)</a>
        </div>
        <?php if ($_SESSION['user_is_admin']): ?>
        <div class="create-post">
            <button type="button"
                onclick="window.location.href='../post/post.php'"
                class="btn btn-primary">Create post</button>
        </div>
        <?php endif; ?>
    </div>

    <div class="row">
        <div class="categories">
            <a href="./blog.php">
                <button type="button" class="btn btn-default">All</button></a>
            <?php if (mysqli_num_rows($categories_result) > 0): ?>
            <?php while($category = mysqli_fetch_assoc($categories_result)): ?>
            <a href="./blog.php?category=<?= $category["category_id"] ?>">
                <button type="button"
                    class="btn btn-default"><?= $category["category_name"]; ?></button></a>
            <?php endwhile; ?>
            <?php else: ?>
            No categories
            <?php endif; ?>
        </div>

        <div class="leftcolumn">
            <?php if (mysqli_num_rows($posts_result) > 0): ?>
            <?php while($post = mysqli_fetch_assoc($posts_result)): ?>
            <div class="card">
                <div class="header-card">
                    <div>
                        <h2><?= $post["post_name"]; ?></h2>
                        <?php
                        $conn = connect_database();
                        $category_sql = "SELECT category_name FROM $table_categories WHERE category_id = '" . $post["category_id"] . "';";
                        $category_result = mysqli_query($conn, $category_sql);
                        disconnect_db($conn);
                        ?>
                        <?php if (mysqli_num_rows($categories_result) > 0): ?>
                        <?php while($post_category = mysqli_fetch_assoc($category_result)): ?>
                        <a
                            href="./blog.php?category=<?= $post["category_id"]; ?>">
                            <button type="button"
                                class="btn btn-default"><?= $post_category["category_name"]; ?></button></a>
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>

                    <?php if ($_SESSION['user_is_admin']): ?>
                    <div class="card-actions">
                        <a href="../post/post.php?id=<?= $post["post_id"]; ?>"
                            type="button" class="btn btn-warning">Edit</a>
                        <a href="./actions/delete.php?id=<?= $post["post_id"]; ?>"
                            type="button"
                            onclick="return confirm('Delete post?')"
                            class="btn btn-danger"><i
                                class="fas fa-trash"></i></a>
                    </div>
                    <?php endif; ?>
                </div>
                <h5>Post creation:
                    <?= date_format(new DateTime($post["post_created_at"]),"d/m/Y H:i:s") ?>
                    (Last updated:
                    <?= date_format(new DateTime($post["post_edited_at"]),"d/m/Y H:i:s") ?>)
                </h5>
                <div class="fakeimg" style="height:200px;">
                    <img src="<?= '../' . $post["post_image"]; ?>">
                </div>
                <p><?= $post["post_description"]; ?></p>
            </div>
            <?php endwhile; ?>
            <?php else: ?>
            <div class="d-flex p-3 m-3 justify-content-center"
                style="background-color: #c3c3c3;">
                No posts
            </div>
            <?php endif; ?>
        </div>

        <div class="rightcolumn">
            <div class="card">
                <h2>About Me</h2>
                <div class="fakeimg" style="height:100px;"><img
                        src="../../img/admin.png"></div>
                <p>Some text about me in culpa qui officia deserunt mollit
                    anim..</p>
            </div>
        </div>
    </div>

    <div class="footer">
        <h5>Blog feito em PHP por Leonardo Zanotti</h5>
        <img src="../../img/footer-img.jpeg" alt="Footer" />
    </div>
</body>

</html>